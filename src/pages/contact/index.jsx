import Navbar from "../../components/molecules/navbar";
import Footer from "../../components/molecules/footer";

import FormField from "../../components/molecules/form-field";

import styled from "styled-components";

const Contact = () => {
  return (
    <Page>
      <Navbar />
      <main>
        <form>
          <h1>Fale Conosco</h1>
          <FormField labelName="nome" fieldName="Nome" />

          <FormField labelName="email" fieldName="Email" />

          <fieldset>
            <legend>Digite sua Mensagem aqui:</legend>
            <FormField labelName="mensagem" />
          </fieldset>
          <button>Enviar</button>
        </form>
      </main>
      <Footer />
    </Page>
  );
};

export default Contact;

const Page = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;

  main {
    margin: 120px auto;

    form {
      box-sizing: border-box;
      width: 70vw;
      height: 70vh;
      margin: 0 auto;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
      border-left: 1px solid #7f7f7f;
      border-right: 1px solid #7f7f7f;
    }

    h1 {
      color: #231f20;
    }

    fieldset {
      width: 300px;
      color: #231f20;
    }

    input#mensagem {
      height: 150px;
      width: 280px;
      border: none;
    }

    button {
      font-size: 1.3rem;
      margin-top: 20px;
      color: #231f20;
    }
  }
`;
