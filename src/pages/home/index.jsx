import Navbar from "../../components/molecules/navbar";
import Footer from "../../components/molecules/footer";

import styled from "styled-components";

import Title from "../../components/atoms/title";

const Home = () => {
  return (
    <Page>
      <Navbar />
      <main>
        <Title title="O Início" />

        <section>
          <fugure>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSV-mpeDpFNBwfGhjUzMCO8A_snH1E-8ngZiA&usqp=CAU"
              alt="Orville Gibson"
            />
            <figcaption>
              Orville Gibson - Fundador da Gibson Brands, Inc.
            </figcaption>
          </fugure>
          <p>
            A Gibson Brands, Inc. foi uma das maiores empresas fabricante de
            guitarras do mundo. Ela também é dona de outros instrumentos que são
            vendidos sob uma variedade de marcas diferentes. Foi fundada em
            Kalamazoo, Michigan, e está atualmente em Nashville no Tennessee.
            Com uma história de mais de cem anos, Gibson tem sido uma das
            companhias mais notórias no desenvolvimento da viola e da guitarra
            elétrica e construiu ao longo de todos esses anos um dos modelos de
            guitarras mais icônicos do mundo, a Gibson Les Paul. Muitos de seus
            instrumentos continuam se valorizando ainda mais no mercado e
            algumas Gibsons estão entre as guitarras mais procuradas por
            colecionadores.
          </p>
        </section>
        <p>
          A empresa foi fundada por Orville Gibson, que produzia bandolim em
          Kalamazoo, no final dos anos 1890. Gibson inventou as guitarras
          semiacústicas usando o mesmo tipo de tampo arqueado encontrado nos
          violinos. A partir de 1930, a companhia também tinha feito violões com
          tampo, como também foi uma das primeiras a disponibilizar no mercado
          guitarras semiacústicas usadas e popularizadas por Charlie Christian.
          Gibson esteve na vanguarda da inovação com relação às guitarras
          acústicas, especialmente na era da Big Band de 1930. A Gibson Super
          400 foi amplamente imitada. No começo dos anos 50, Gibson introduziu
          sua primeira guitarra de corpo sólido e em 1952 começou a produzir seu
          modelo de guitarra mais popular, a Les Paul, projetada por Ted McCarty
          e Les Paul; Depois de ser comprada pela Norlin Corporation no final
          dos anos 60, a qualidade e a fortuna da Gibson começou a declinar até
          1986, quando a companhia foi adquirida por seus proprietários atuais.
        </p>

        <Title title="O Som Único" />

        <p>
          Não é a toa que a Gibson é uma das preferidas dos guitarristas
          referência por ai. Confira no vídeo abaixo do canal{" "}
          <a href="https://www.youtube.com/channel/UCj1Jtb8xLUzFAm8J-Q1e1MQ">
            samuraiguitarist
          </a>{" "}
          os diversos timbres que a marca oferece:
        </p>

        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/LfPciLK5CBM"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
          title="Playing all the Gibson Guitars"
        ></iframe>
      </main>
      <Footer />
    </Page>
  );
};

export default Home;

const Page = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;

  main {
    width: 85%;
    margin: 120px auto;

    section {
      padding: 15px 0;
      display: flex;
      justify-content: space-around;
    }

    section p {
      width: 600px;
    }

    img {
      width: 400px;
    }

    figcaption {
      font-size: 0.8rem;
    }

    a {
      color: #7f7f7f;
      text-decoration: none;
    }
    a:hover {
      color: #231f20;
    }
  }
`;
