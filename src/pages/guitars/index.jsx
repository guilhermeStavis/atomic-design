import Navbar from "../../components/molecules/navbar";
import Footer from "../../components/molecules/footer";
import GuitarTable from "../../components/organisms/guitar-table";

import styled from "styled-components";

import Title from "../../components/atoms/title";

const Guitars = () => {
  return (
    <Page>
      <Navbar />
      <main>
        <Title title="Guitarras da nossa coleção" />
        <GuitarTable />
      </main>
      <Footer />
    </Page>
  );
};

export default Guitars;

const Page = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;

  main {
    margin: 120px auto;

    h1 {
      color: #231f20;
    }

    div {
      display: flex;
      flex-flow: row wrap;
      justify-content: space-around;
    }
  }
`;
