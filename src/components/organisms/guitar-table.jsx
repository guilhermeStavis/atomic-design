import GuitarBox from "../molecules/guitar-box";

const GuitarTable = () => {
  return (
    <div>
      <GuitarBox
        guitarName="Les Paul Standard '50s"
        guitarUrl="https://static.gibson.com/product-images/USA/USAUBC849/Gold%20Top/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="Les Paul Junior"
        guitarUrl="https://static.gibson.com/product-images/USA/USARLS446/Ebony/front-banner-1600_900.png"
      />
      <GuitarBox
        guitarName="SG Junior"
        guitarUrl="https://static.gibson.com/product-images/USA/USAFG1643/Vintage%20Cherry/front-banner-1600_900.png"
      />
      <GuitarBox
        guitarName="Les Paul Standard '50s P-90"
        guitarUrl="https://static.gibson.com/product-images/USA/USA85B326/Gold%20Top/front-banner-1600_900.png"
      />
      <GuitarBox
        guitarName="Les Paul Standard '60s"
        guitarUrl="https://static.gibson.com/product-images/USA/USA1R6524/Iced%20Tea/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="Les Paul Special"
        guitarUrl="https://static.gibson.com/product-images/USA/USA2KP357/TV%20Yellow/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="SG Standard '61 Maestro Vibrola"
        guitarUrl="https://static.gibson.com/product-images/USA/USAD9T241/Vintage%20Cherry/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="SG Standard '61 Sideways Vibrola"
        guitarUrl="https://static.gibson.com/product-images/USA/USAKA9147/Vintage%20Cherry/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="SG Standard '61"
        guitarUrl="https://static.gibson.com/product-images/USA/USAEDH414/Vintage%20Cherry/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="SG Special"
        guitarUrl="https://static.gibson.com/product-images/USA/USAFN1750/Faded%20Pelham%20Blue/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="ES-335"
        guitarUrl="https://static.gibson.com/product-images/USA/USAVLJ627/Sixties%20Cherry/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="Firebird"
        guitarUrl="https://static.gibson.com/product-images/USA/USA5KL895/Tobacco%20Burst/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="Flying V"
        guitarUrl="https://static.gibson.com/product-images/USA/USAM1U838/Antique%20Natural/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="Explorer"
        guitarUrl="https://static.gibson.com/product-images/USA/USAI3T44/Antique%20Natural/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="70s Flying V"
        guitarUrl="https://static.gibson.com/product-images/USA/USADW9572/Classic%20White/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="70s Explorer"
        guitarUrl="https://static.gibson.com/product-images/USA/USAENQ873/Classic%20White/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="Thunderbird Bass"
        guitarUrl="https://static.gibson.com/product-images/USA/USAKUC232/Tobacco%20Burst/front-banner-1600_900.png"
      />

      <GuitarBox
        guitarName="SG Standard Bass"
        guitarUrl="https://static.gibson.com/product-images/USA/USABCH291/Heritage%20Cherry/front-banner-1600_900.png"
      />
    </div>
  );
};

export default GuitarTable;
