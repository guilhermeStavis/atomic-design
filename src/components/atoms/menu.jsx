import { Link } from "react-router-dom";

const Menu = () => {
  return (
    <div className="menu">
      <Link to="/">Home</Link>
      <Link to="/guitars">Nossas Guitarras</Link>
      <Link to="/contact">Fale Conosco</Link>
    </div>
  );
};

export default Menu;
