const Input = ({ type, name, id, placeholder }) => {
  return <input type={type} name={name} id={id} placeholder={placeholder} />;
};

export default Input;
