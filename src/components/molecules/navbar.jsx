import styled from "styled-components";

import Image from "../atoms/image";
import Menu from "../atoms/menu";

const Navbar = () => {
  return (
    <Nav>
      <Image
        src="https://static.gibson.com/gibson-web/branding/Header-Gibson-Logo.png"
        alt="Gibson logo"
      />
      <Menu />
    </Nav>
  );
};

export default Navbar;

const Nav = styled.header`
  position: fixed;
  top: 0;
  width: 100%;
  background-color: #fff;
  margin-top: 0;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #7f7f7f;
  box-shadow: 0px 2px 30px #555;

  .menu {
    margin-right: 110px;
    width: 800px;
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-around;
    align-items: center;
  }

  a {
    color: #7f7f7f;
    text-decoration: none;
    font-size: 1.5rem;
  }
  a:hover {
    color: #231f20;
  }
`;
