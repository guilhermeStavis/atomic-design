import Input from "../atoms/input";

const FormField = ({ labelName, fieldName }) => {
  return (
    <div>
      <Input
        type="text"
        name={labelName}
        id={labelName}
        placeholder={fieldName}
      />
    </div>
  );
};

export default FormField;
