import styled from "styled-components";

import Image from "../atoms/image";
import Subtitle from "../atoms/subtitle";

const GuitarBox = ({ guitarName, guitarUrl }) => {
  return (
    <Box>
      <Subtitle subtitle={guitarName} />
      <Image src={guitarUrl} alt={guitarName} />
    </Box>
  );
};

export default GuitarBox;

const Box = styled.div`
  width: 350px;
  height: 250px;

  margin-top: 20px;

  h2 {
    color: #231f20;
  }
  img {
    width: 300px;
  }
`;
