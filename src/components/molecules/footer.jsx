import styled from "styled-components";

const Footer = () => {
  return (
    <Foot>
      <div>
        <p>
          <a href="https://www.gibson.com/">Gibson</a> -{" "}
          <a href="https://www.linkedin.com/in/guilherme-stavis/">
            Guilherme Stavis
          </a>{" "}
          - <a href="https://kenzie.com.br/">Kenzie Academy Brasil</a> - 2021
          &copy;
        </p>
      </div>
    </Foot>
  );
};

export default Footer;

const Foot = styled.footer`
  div {
    border-top: 1px solid #7f7f7f;
  }

  div p {
    font-size: 1.2rem;
  }
  a {
    color: #7f7f7f;
    text-decoration: none;
  }
  a:hover {
    color: #231f20;
  }
`;
