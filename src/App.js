import "./App.css";
import Home from "./pages/home";
import Guitars from "./pages/guitars";
import Contact from "./pages/contact";

import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>

        <Route path="/guitars">
          <Guitars />
        </Route>

        <Route path="/contact">
          <Contact />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
